---
layout: post
title:  "Setting up a dockerized LEMP stack (Linux, NGINX, MySQL, PHP) project"
date:   2021-07-10 18:30:00 +0200
categories: guides update
tags: php docker backend mysql databases
---
I am about to start working on a side project for which I will be needing an API. The <a href="https://lemp.io/" target="_blank">LEMP</a> stack is what I feel more comfortable working with but every time I set one up I have a hard time remembering some of the details involved. So I said to myself: what if I document it and use the post for future reference? It may even help someone else!

tl;dr: 
<a href="https://gitlab.com/jgarcese/dockerized-lemp-stack" target="_blank">https://gitlab.com/jgarcese/dockerized-lemp-stack</a>

## What we will be building
Three Docker containers from the following Images: `php:8.0.8-fpm-alpine3.13`, `nginx:1.20.1-alpine` and `mariadb:latest`.

The PHP container will be running Symfony Framework 5.3 with PHPUnit and Xdebug. Doctrine DBAL will be used for database management and Doctrine Migrations will take care of <a href="https://en.wikipedia.org/wiki/Schema_migration" target="_blank">schema versioning</a>.

PhpStorm support for Xdebug and PHPUnit will also be set up.

## Requirements
<a href="https://docs.docker.com/get-docker/" target="_blank">Docker</a> & <a href="https://docs.docker.com/compose/install/ " target="_blank">Docker Compose</a>, that's it. Every other dependency will be installed, configured and run inside the containers.

## 1. The PHP container
Start by creating a folder for your project.

```console
$ mkdir project && cd project
```
Create a new file in your favorite text editor named `docker-compose.yml` to start configuring our first container. It should look like this:

```yaml
version: '3.9'

services:
  api:   
    build:
      context: .
      dockerfile: docker/php-fpm/Dockerfile
    container_name: project.api
    volumes:
      - .:/app:cached

```
In the project's root, create a folder called `docker` and one inside it called `php-fpm`. We will place two files in the `php-fpm` folder: `DockerFile` and `settings.ini`.

>A Dockerfile is a text document that contains all the commands a user could call on the command line to assemble an image. Using docker build users can create an automated build that executes several command-line instructions in succession.

`settings.ini` will hold the variables needed by Xdebug. It will be copied to the PHP volume while the image is being assembled.

### Dockerfile
```docker
FROM php:8.0.8-fpm-alpine3.13

RUN apk add --update --no-cache oniguruma-dev $PHPIZE_DEPS; \
pecl install xdebug && docker-php-ext-enable xdebug; \
pecl install apcu && docker-php-ext-enable apcu; \
docker-php-ext-install mbstring pdo_mysql;

RUN apk --no-cache add ca-certificates wget bash \
&& wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
&& wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.29-r0/glibc-2.29-r0.apk \
&& apk add glibc-2.29-r0.apk

ARG COMPOSER_VERSION=2.1.3
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV PHP_IDE_CONFIG "serverName=docker-server"

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer --version=${COMPOSER_VERSION};

COPY ./docker/php-fpm/settings.ini /usr/local/etc/php/php.ini

RUN apk add --no-cache git openssh

RUN addgroup -g 1000 -S phpuser && \
adduser -u 1000 -S phpuser -G phpuser

USER phpuser
WORKDIR /app

```
### settings.ini
```shell
##For Linux
xdebug.client_host=172.17.0.1
##For Mac
#xdebug.client_host="docker.for.mac.localhost"
xdebug.start_with_request=trigger
xdebug.mode=debug
xdebug.idekey=PHPSTORM
xdebug.client_port=9000
xdebug.discover_client_host=0
xdebug.output_dir=/var/www/html/xdebug/
```
Comment or uncomment lines 2 and 4 depending on your Operating System.


That is all our image needs to become a container! We are one command away from building, but first let's create one last file in the project's root that will simplify using commands or automating tasks and make our lives easier: the `Makefile`.
```shell
init:
	docker-compose up --build -d
```
This is how the folder structure should be looking so far:
```console
project 
├── docker
│   └── php-fpm
│       └── Dockerfile
│       └── settings.ini
│── docker-compose.yml
│── Makefile
``` 
In the project's root, type this command:
```console
$ make init
```
The build will take a couple minutes to complete and you will be presented with a message like this.
```console
Creating project.api ... done
```
The `docker ps` command shows a list of active containers, in which the new one (project.api) should be present. Note the port it uses by default: 9000, we will need that later.
```console
$ docker ps

CONTAINER ID   IMAGE              COMMAND                  CREATED          STATUS          PORTS      NAMES
af5103b523a2   project_api        "docker-php-entrypoi…"   50 seconds ago   Up 50 seconds   9000/tcp   project.api
```
We now need to get inside the container to continue with the dependency installation. We will be able to do it by adding and executing a new `Makefile` command.
```shell
init:
	docker-compose up --build -d
terminal:
	docker exec -it project.api bash
```

### Symfony installation
Composer will take care of installing the Symfony Framework. We are choosing a minimal installation (skeleton) to reduce bloat and make sure that our API only has to bother about what it really needs. Enter the container:

```console
$ make terminal
```
On the bash prompt, issue the following command:
```console
bash-5.1$ composer create-project symfony/skeleton tmp
```
Notice how we asked Composer to place Symfony inside a temporary directory, don't worry. The reason behind it is that I am used to maintaining a folder structure where every relevant directory is on the outmost level. Symfony won't install if the directory is not empty, and we have the files that we have been using to build the container in it.

Once it has finished, just move everything from the tmp directory to the current directory and delete the temporary one. A 'resource busy' warning will pop out, press enter and proceed with the rm command.
```console
bash-5.1$ mv tmp/{.,}* .
mv: can't rename 'tmp/.': Resource busy
mv: overwrite './..'? 
bash-5.1$ rm -rf tmp
bash-5.1$ 
```
Next we will be installing PhpUnit and PhpUnit-Bridge
```console
bash-5.1$ composer require --dev phpunit/phpunit
```
```console
bash-5.1$ composer require --dev symfony/phpunit-bridge
```
And that's all we need to do inside the PHP container! Before leaving, issue the following command:
```console
bash-5.1$ pwd
/app
```
The `pwd` command, or 'print work directory', writes the full pathname of the current working directory to the standard output. Note that it is '/app'.

Lets exit the shell and start with PhpStorm and Xdebug configuration.
```console
bash-5.1$ exit 
```
When you exit the container, you will see a bunch of new files and directories in your project folder.

![structure](../../assets/img/lamp/folders.png)

It has to do with this section of the docker-compose.yml file
```yaml
volumes:
      - .:/app:cached
```
When the container is up, any changes made to the directory where the docker-compose.yml file is located (that's what the dot means) also happen in the container's '/app' directory, and viceversa.

### Setup PhpStorm for testing and debugging
There are four places in which to configure the ide to work with the Docker container.
### The CLI Interpreter
![cli_interpreter](../../assets/img/lamp/CLI_Interpreter.png)
Found under Settings / PHP. Click the three dots on the right, then the plus sign on the top left of the new window and select the 'From Docker, Vagrant, Vm ...' option. Mark the 'Docker Compose' radio button and in the 'Service' drop down select 'api' (the service name we set up in the docker-compose.yml). Click OK and we are done!
### Composer
![composer](../../assets/img/lamp/composer.png)
Found under Settings / PHP / Composer. Point to the correct path. To avoid Language Level mismatches, open composer.json and set "php" to ">=8.0".
### PhpUnit
![phpunit](../../assets/img/lamp/phpunit.png)
Found under Settings / PHP / Test Frameworks. Click this other plus sign and select 'PhpUnit from remote interpreter'. Click the 'Path to phpunit.phar' radio button, fill in the input with '/app/vendor/bin/phpunit' and hit the refresh icon to make sure it updates successfully. To complete the Test Frameworks setup, enter '/app/phpunit.xml.dist' as the path to the default configuration file.
### Xdebug
![xdebug](../../assets/img/lamp/xdebug.png)
Found under Settings / PHP / Debug / Servers. Once again, click the plus sign. It is important that you set the name to 'docker-server' like in the Dockerfile. Use 'localhost' for host, 9000 for port, click the checkbox and set the path mapping from the project folder to '/app'.

And we are done! Let's add some more entries to the `Makefile` before wrapping up the Php container section.

```shell
init: ## Build images and install composer deps
	docker-compose up --build -d
	$(MAKE) install-dependencies

up: ## Up all services
	docker-compose up -d

down: ## Down all services
	docker-compose down

terminal: ## Enter the php container shell
	docker exec -it project.api bash

install-dependencies: ### install composer dependencies
	docker exec project.api composer install

test: ## Run test suites
	docker exec project.api ./vendor/bin/phpunit
```

## 2. The NGINX container

Let's start by adding the new service to the `docker-compose.yml`
```yaml
version: '3.9'

services:
  api:
    build:
      context: .
      dockerfile: docker/php-fpm/Dockerfile
    container_name: project.api
    volumes:
      - .:/app:cached
  nginx:
    build:
      context: .
      dockerfile: docker/nginx/Dockerfile
    container_name: project.nginx
    ports:
      - "8080:80"
    volumes:
      - .:/app
```
The container will be listening for incoming connections on port 8080 and redirecting them to internal port 80, which is the port we will be setting for the NGINX server.


Like with the PHP container, we will need a folder named `nginx` inside the `docker` one to store the build config files: a simple `Dockerfile` and one that tells NGNIX how to interact with our Symfony, which we will call `project.conf`. This is how the `docker` folder should look like:
```console
docker 
├── php-fpm
│   └── Dockerfile
│   └── settings.ini
│── nginx 
│   └── Dockerfile
│   └── project.conf 
``` 
## Dockerfile
```docker
FROM nginx:1.20.1-alpine

COPY docker/nginx/project.conf /etc/nginx/conf.d/default.conf
```
## project.conf
```conf
server {
listen 80;
server_name localhost;
root /app/public;
fastcgi_buffers 16 16k;
fastcgi_buffer_size 32k;

    location / {
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index\.php(/|$) {
        fastcgi_pass api:9000;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
    }
    location ~ \.php$ {
        return 404;
    }

    error_log /var/log/nginx/error.log;
    access_log /var/log/nginx/access.log;
}

```
In addition to the port that will receive incoming connections, NGINX needs to know the name of the service and port to connect to. The service in our case would be 'api' and the port, as we saw earlier, 9000.

Ok then, let's first shut down our php container
```console
$ make down
docker-compose down
Stopping project.api ... done
Removing project.api ... done
Removing network project_default
```
As we need to build the NGINX image from scratch, lets make use of the 'init' command in the `Makefile`
```console
$ make init 
Creating project.nginx ... done
Creating project.api   ... done
$ docker ps
CONTAINER ID   IMAGE           COMMAND                  CREATED          STATUS          PORTS                                   NAMES
7e7367a3ba2e   project_api     "docker-php-entrypoi…"   49 seconds ago   Up 49 seconds   9000/tcp                                project.api
46a0df0bbb00   project_nginx   "/docker-entrypoint.…"   49 seconds ago   Up 49 seconds   0.0.0.0:8080->80/tcp, :::8080->80/tcp   project.nginx
```
Looking good! If we open our browser and navigate to `http://localhost:8080/` we should be greeted by Symfony's welcome screen.

![symfony](../../assets/img/lamp/symfony.png)

## First test and controller
Let's write our first controller to test PhpStorm is working as intended. To keep things simple, we will install a new dependency with composer that will help us test the controller.

> The BrowserKit component simulates the behavior of a web browser, allowing you to make requests, click on links and submit forms programmatically.

```console
$ make terminal 
bash-5.1$ composer require --dev symfony/browser-kit
bash-5.1$ exit
```
Let's create our first test and see what happens: 
`tests/Infrastructure/HealthzControllerTest.php`
```php
<?php

declare(strict_types=1);

namespace App\Tests\Infrastructure;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use function PHPUnit\Framework\assertEquals;

class HealthzControllerTest extends WebTestCase
{
    private const HTTP_OK = 200;

    /** @test */
    public function shouldReturnStatusCode200ok(): void
    {
        $client = static::createClient();
        $client->request('GET', '/healthz');

        $responseStatusCode = $client->getResponse()->getStatusCode();

        $expectedResponseStatusCode = self::HTTP_OK;
        assertEquals($expectedResponseStatusCode, $responseStatusCode);
    }
}


```
PhpStorm runs the test just fine, and we have a Red!
![phpunit](../../assets/img/lamp/testFailedPhpstorm.png)

If you are not using the ide or prefer running the tests from the console (faster), remember the `Makefile` command we added before.
```console
$ make test
```

'No route found', we'll start from there.

`config/routes.yaml`
```yaml
healthz:
  path: /healthz
  controller: App\Controller\Infrastructure\HealthzController::index

```
We now need to write the controller, as the test result suggests
> "The controller for URI "/healthz" is not callable: Controller "App\Controller\Infrastructure\HealthzController" does neither exist as service nor as class."

`src/Controller/Infrastructure/HealthzController.php`
```php
<?php

namespace App\Controller\Infrastructure;

use Symfony\Component\HttpFoundation\Response;

class HealthzController
{
    public function index(): Response
    {
        return new Response('ok', 200);
    }
}

```

We re-run the tests ... et voilà, green! No need to declare it as a controller in `config\services.yaml`, Symfony is smart enough to get the information it needs from the route definition. 

![testOk](../../assets/img/lamp/testOk.png)

Let's see what happens when using a real browser other than the simulated by the Symfony component.
![healthzy](../../assets/img/lamp/healthzy.png)

## Xdebug browser extension
On of the headaches I was referring to when starting this post is making Xdebug work from the web browser. Not today though! We will need the <a href="https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc" target="_blank">Xdebug helper</a> extension.

Once installed, right-click the bug icon on your browser toolbar, go to options and make sure you are using the correct IDE key for your software. In out case, that would be 'PHPSTORM', as we configured in `settings.ini` when building the PHP image. Exit the configuration screen, left-click the bug and click 'Debug' so it turns green.

Now all requests made by that browser tab will include a Cookie with the following info: `XDEBUG_SESSION=PHPSTORM`. If PhpStorm is debugging, it will listen for connections and stop when a breakpoint is reached.

Let's just try. Place a breakpoint on line 11 of the HealthzController and start listening for connections by clicking the red telephone on the toolbar so it turns green and starts listening for connections.

![debugger](../../assets/img/lamp/debugger.png)

Navigate to `http://localhost:8080/healthz` in the browser with the Xdebug helper bug in green state. PhpStorm should claim window focus and present you with the debugging interface!

![breakpoint](../../assets/img/lamp/breakpoint.png)


## 3. The MySQL container
As always, add the new service to the `docker-compose.yml`. In this case we will be using an image directly, so no extra configuration will be needed for building it.
```yaml
version: '3.9'

services:
  api:
    build:
      context: .
      dockerfile: docker/php-fpm/Dockerfile
    container_name: project.api
    volumes:
      - .:/app:cached
  nginx:
    build:
      context: .
      dockerfile: docker/nginx/Dockerfile
    container_name: project.nginx
    ports:
      - "8080:80"
    volumes:
      - .:/app
  mysql:
    image: mariadb:latest
    env_file:
      - .env
    container_name: project.mysql
    ports:
      - 33066:3306

```
We need to set some environment variables to use with our database. Open `.env` and append the following to the file:

```config
MYSQL_HOST=project.mysql
MYSQL_DATABASE=project_db
MYSQL_USER=user
MYSQL_PASSWORD=password
MYSQL_ROOT_PASSWORD=password
```
Initialize all containers and check that the MySQL one is up and running.
```console
$ make init
docker-compose up -d
Creating network "project_default" with the default driver
Creating project.api   ... done
Creating project.nginx ... done
Creating project.mysql ... done
$ docker ps
CONTAINER ID   IMAGE            COMMAND                  CREATED         STATUS         PORTS                                         NAMES
6493250cc133   mariadb:latest   "docker-entrypoint.s…"   3 seconds ago   Up 2 seconds   0.0.0.0:33066->3306/tcp, :::33066->3306/tcp   project.mysql
51127dde7ebd   project_nginx    "/docker-entrypoint.…"   3 seconds ago   Up 2 seconds   0.0.0.0:8080->80/tcp, :::8080->80/tcp         project.nginx
5672f5f17cc1   project_api      "docker-php-entrypoi…"   3 seconds ago   Up 2 seconds   9000/tcp                                      project.api
```
Now lets get in the project.mysql container and check if everything is working
```console
$ docker exec -it project.mysql bash
root@2d54993b8525:/# mysql -uuser -ppassword
MariaDB [project_db]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| project_db         |
+--------------------+
2 rows in set (0.001 sec)

MariaDB [project_db]> 

```
## Migrations
We will need <a href="https://www.doctrine-project.org/projects/doctrine-migrations/en/3.0/reference/introduction.html#introduction" target="_blank">Doctrine Migrations</a> installed in our api service, so make terminal into it and install the package. That will take care of installing Doctrine DBAL too.
```console
$ make terminal
bash-5.1$ composer require --dev doctrine/migrations 
Nothing to unpack
bash-5.1$ exit
```
We now need to create a new directory in the root folder which we will name `migrations`. Inside we will write the migrations' configuration file, which Symfony will use to keep track of the migrations that are executed using its own table.

`migrations\migrations.php`
```php
<?php

return [
    'table_storage' => [
        'table_name' => 'doctrine_migration_versions',
        'version_column_name' => 'version',
        'version_column_length' => 191,
        'executed_at_column_name' => 'executed_at',
        'execution_time_column_name' => 'execution_time',
    ],

    'migrations_paths' => [
        'Migrations' => './migrations',
    ],

    'all_or_nothing' => true,
    'check_database_platform' => true,
    'organize_migrations' => 'none',
];

```
Throw this other config file into the `config` directory so Doctrine can manage console commands and handle the process.

`config\cli-config.php`
```php
<?php

require 'vendor/autoload.php';

use Doctrine\DBAL\DriverManager;
use Doctrine\Migrations\Configuration\Connection\ExistingConnection;
use Doctrine\Migrations\Configuration\Migration\PhpFile;
use Doctrine\Migrations\DependencyFactory;
use Symfony\Component\Dotenv\Dotenv;

(new Dotenv())->bootEnv(dirname(__DIR__).'/.env');

$config = new PhpFile('./migrations/migrations.php');

$conn = DriverManager::getConnection([
    'dbname' => $_ENV['MYSQL_DATABASE'],
    'user' => $_ENV['MYSQL_USER'],
    'password' => $_ENV['MYSQL_PASSWORD'],
    'host' => $_ENV['MYSQL_HOST'],
    'driver' => 'pdo_mysql',
    'charset' => 'utf8mb4',
]);

return DependencyFactory::fromConnection($config, new ExistingConnection($conn));

```
Update `Makefile` with some more commands to make migrations easier
```shell
init: ## Build images and install composer deps
	docker-compose up --build -d
	$(MAKE) install-dependencies

up: ## Up all services
	docker-compose up -d

down: ## Down all services
	docker-compose down

terminal: ## Enter the php container shell
	docker exec -it project.api bash

install-dependencies: ### install composer dependencies
	docker exec project.api composer install

test: ## Run test suites
	docker exec project.api ./vendor/bin/phpunit

new-migration: ## Generate a new migration
	docker exec project.api ./vendor/bin/doctrine-migrations generate

run-db-migrations: ## Run migrations
	docker exec project.api ./vendor/bin/doctrine-migrations migrate
	
```
We are all set to start using schema versioning in our project. Let's create the first migration and check if everything is working. The following command will create a migration file in the `migrations` directory which Doctrine will use to make changes to the database.

By default, migration file names using the date in which they are created.
```console
$ make new-migration
```
We will edit the file to add a new `users` table to the database, with two columns: `email` and `password`.
```php
<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210714175009 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added users table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            <<<SQL
            CREATE TABLE `users` (
            `id` char(36) NOT NULL DEFAULT '',
            `email` char(255) NOT NULL,
            `password` char(255) NOT NULL,
            PRIMARY KEY (`email`),
            CONSTRAINT uc_email UNIQUE (`email`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            SQL
        );
    }

    public function down(Schema $schema): void
    {
    }
}


```
To run the new migration we just need to run the `run-db-migrations` command from the `Makefile`. It will check the migrations directory for any new ones, apply them, and log the change to the database.

Next time it's executed, only new migrations will be applied. Knowing this, it would be a great idea to automate the process so it happens every time the container comes up. We will do that in a minute, but first we need a little script that will make sure the connection to the database has been established before running the migrations.

`bin\wait-mysql-connection`
```shell
#!/usr/bin/env sh

source $(pwd)/.env

maxtries=20
until [ $maxtries -eq 0 ] || nc -z -w10 project.mysql 3306
do
  echo "Waiting for database connection..."

  maxtries=$(( maxtries-1 ))

  # wait for 1 seconds before check again
  sleep 1s
done

if [ $maxtries -eq 0 ]; then
  echo >&2 'error: unable to contact MySQL after 20 tries'
  exit 1
fi

echo 'MySQL connection successful'
exit 0

```
Make the script executable
```console
$ chmod +x bin/wait-mysql-connection
```
The following will be our last update to the `Makefile`
```shell
init: ## Build and update images 
	docker-compose up --build -d
	$(MAKE) install-dependencies
	$(MAKE) run-db-migrations

up: ## Up all services
	docker-compose up -d
	$(MAKE) run-db-migrations

down: ## Down all services
	docker-compose down

terminal: ## Enter the php container shell
	docker exec -it project.api bash

install-dependencies: ### install composer dependencies
	docker exec project.api composer install

test: ## Run test suites
	docker exec project.api ./vendor/bin/phpunit

new-migration: ## Generate a new migration
	docker exec project.api ./vendor/bin/doctrine-migrations generate

run-db-migrations: wait-mysql-connection ## Run migrations
	docker exec project.api ./vendor/bin/doctrine-migrations migrate

wait-mysql-connection: ## Wait for MySql to be ready
	docker-compose run api ./bin/wait-mysql-connection

```

That would be all. make down, make up, and check that the migration has been correctly applied at startup.

```console
$ docker exec -it project.mysql bash
root@4378b72846c5:/# mysql -uuser -ppassword
MariaDB [(none)]> use project_db;
Database changed
MariaDB [project_db]> show tables;
+-----------------------------+
| Tables_in_project_db        |
+-----------------------------+
| doctrine_migration_versions |
| users                       |
+-----------------------------+
2 rows in set (0.001 sec)
```
