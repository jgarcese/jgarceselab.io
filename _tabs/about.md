---
title: About
icon: fas fa-info
order: 4
---

***Launched*** this site to serve me as a reference for the development related tasks that I am learning and using, as well as a way to reinforce them. As a coworker likes to say:

>*“I hear and I forget. I see and I remember. I do and I understand.”*

Add some blogging to the mix!

I write mostly about web development. If you are a programmer I hope you may benefit from some of the information posted around here. Please let me know if it happens and also drop a comment if you find anything wrong, you think it could be improved or maybe done differently.

![me presenting](../assets/img/me/metoo.jpeg){:width="400px" } 

I started my Software Engineering career in 2019 by enrolling in [Skylab Coders Academy ](https://www.skylabcoders.com/en "Skylab's Homepage")'s JavaScript-based full stack web development bootcamp in Barcelona.

I then joined [Lifull Connect](https://www.lifullconnect.com/about-us/ "Lifull's Site"), where I entered an in-house apprenticeship program conducted by [CoKaiDo](https://cokaido.com/ "CoKaiDo's web page") during which I was introduced to best practices and modern software development methodologies, taught mostly in Java.

In the B2B team I am part of we develop an application for our clients in the real estate market. We use PHP (Symfony) for the backend and JavaScript (React) on the frontend, with an Agile (SCRUM) and [Extreme Programming](https://www.agilealliance.org/glossary/xp "Agile Alliance") mindset.

We are [hiring](https://www.lifullconnect.com/career/job_20210216113736_6pd9bcv3v44sxewk-full-stack-developer-b2b/ "Join us!"), by the way ;)
